# PEC4 - Diseño de experiencia de usuario e interfaces



## Unity XR - Interfaz diegética

A continuación, se presenta una propuesta de juego de realidad virtual (RV) en el cual se intentarán introducir el máximo de elementos diegéticos. El alcance de este trabajo solo abarca la implementación del menú principal del juego.

Elementos del menú principal:
 + Interacción para empezar una partida.
 + Interacción para modificar la dificultad del juego.
 + Interacción para pausar o reproducir la música del juego.
 + Interacción con algunos elementos de la tienda (Comprar y equipar).
 + Visualizar las monedas del juego disponibles por el usuario.

![Screenshot del menú principal](Imagenes/MainMenu.png)

### Demostración de los elementos implementados

- [ ] [Vídeo de demostración](https://youtu.be/RhOEfZEk-gE) (Youtube)


### Referencias

- [ ] Assets 3D: [Polygon Prototype - Synty Studios](https://assetstore.unity.com/packages/3d/props/exterior/polygon-prototype-low-poly-3d-art-by-synty-137126)

- [ ] Script AudioManager: [Brackeys Video](https://www.youtube.com/watch?v=6OT43pvUyfY&ab_channel=Brackeys) (Youtube)

- [ ] Audio source: [FiftySounds.com](https://www.fiftysounds.com/es/musica-libre-de-derechos/solo-los-valientes.html)

***
