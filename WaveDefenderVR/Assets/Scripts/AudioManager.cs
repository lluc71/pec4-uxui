using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;

public class AudioManager : MonoBehaviour
{
    [SerializeField]
    public Sound[] sounds;

    public static AudioManager instance;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);

        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.loop = s.loop;
            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
    }

    private void Start()
    {
        //Reproduce la cancion Theme
        PlaySound("Theme"); 
    }

    /**
     * Reproduce la cancion con el nombre @name si se encuentra en nuestra lista de sonidos.
     */
    public void PlaySound(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            Debug.LogWarning("Sound '" + name + "' not found!");
            return;
        }
        s.source.Play();
    }

    /**
     * Pausa o reproduce la cancion dependiendo del parametro @musicOn
     */
    public void SwitchMusicOnOFF()
    {
        AudioListener.pause = !AudioListener.pause;
    }
}
