using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public enum GameMode
{
    FACIL, NORMAL, DIFICIL
}

public enum WeaponRangeType
{
    SIN, CORTO, MEDIO, LARGO
}

public class MainMenuManager : MonoBehaviour
{
    [Header("Difficulty")]
    private GameMode mode = GameMode.NORMAL;
    [SerializeField] private TextMeshProUGUI modeText;

    [Header("Economy")]
    private int coins = 100;
    [SerializeField] private TextMeshProUGUI coinsText;

    private WeaponRangeType weaponType = WeaponRangeType.SIN;

    private void Start()
    {
        coinsText.text = coins.ToString() + "$";
    }

    /**
     * Empieza el juego con la dificultad seleccionada.
     * Por defecto, dificultad normal.
     */
    public void StartGame()
    {
        Debug.Log("Cargando pantalla de Juego... (Modo: " + mode + ")");
        Debug.Log("Alcance del arma: " + weaponType.ToString());
        //SceneManager.LoadScene("Game");
    }

    /**
     * Modifica la dificultad del juego.
     */
    public void ChangeMode()
    {
        switch (mode)
        {
            case GameMode.FACIL: 
                mode = GameMode.NORMAL; 
                break;
            case GameMode.NORMAL: 
                mode = GameMode.DIFICIL; 
                break;
            case GameMode.DIFICIL: 
                mode = GameMode.FACIL; 
                break;
        }
        modeText.text = mode.ToString();
    }

    public int GetCoins()
    {
        return coins;
    }
    
    public void AddCoins(int value)
    {
        coins += value;
        coinsText.text = coins.ToString() + "$";
    }

    public void SetWeaponType(WeaponRangeType type)
    {
        weaponType = type;
    }
}
