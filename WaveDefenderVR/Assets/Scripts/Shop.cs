using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Shop : MonoBehaviour
{
    [SerializeField] private MainMenuManager mainMenuManager;

    [Header("Shop")]
    private bool bought = false;
    [SerializeField] private int coinsCost;
    [SerializeField] private WeaponRangeType weaponType = WeaponRangeType.SIN;
    [SerializeField] private TextMeshProUGUI coinsText;

    public void Start()
    {
        coinsText.text = coinsCost.ToString() + "$";
    }

    public void ChooseWeapon()
    {
        if (bought)
        {
            //Seleccionar esta arma:
            SelectShop();
        } else
        {
            //Comprar arma si hay suficiente dinero:
            if (mainMenuManager.GetCoins() >= coinsCost)
            {
                mainMenuManager.AddCoins(-coinsCost);
                bought = true;
                SelectShop();
            } else
            {
                Debug.Log("No tienes suficiente dinero! Te faltan " + (coinsCost-mainMenuManager.GetCoins()) + "$");
            }
        }
    }

    /**
     * Actualiza la tienda actual como la seleccionada. Las demas tiendas, si estaban compradas, pasaran al estado 0$.
     */
    private void SelectShop()
    {
        Shop[] shops = FindObjectsOfType<Shop>();
        foreach(Shop shop in shops)
        {
            if (shop.IsBought())
            {
                shop.SetCoinsText("0$");
            }
        }

        this.coinsText.text = "Equipped";
        mainMenuManager.SetWeaponType(this.weaponType);

    }

    public bool IsBought()
    {
        return bought;
    }

    public void SetCoinsText(string text)
    {
        coinsText.text = text;
    }
}
